﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Galuguel.Models
{
    public class Context : DbContext
    {
        public Context() : base("DefaultConnection") 
        {
            Database.CreateIfNotExists();
        }

        public DbSet<Pessoa> Pessoas { get; set; }
    }

}