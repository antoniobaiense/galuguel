﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Galuguel.Models
{
    public class Pessoa
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Nome do Arrombado")]
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Idade { get; set; }

    }
}