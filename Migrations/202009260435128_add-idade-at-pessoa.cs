﻿namespace Galuguel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addidadeatpessoa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pessoas", "Idade", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pessoas", "Idade");
        }
    }
}
