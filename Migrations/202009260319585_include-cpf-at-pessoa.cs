﻿namespace Galuguel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class includecpfatpessoa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pessoas", "Cpf", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pessoas", "Cpf");
        }
    }
}
