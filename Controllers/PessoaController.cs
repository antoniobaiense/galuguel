﻿using Galuguel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Galuguel.Controllers
{
    public class PessoaController : Controller
    {
        private Context _context;

        // GET: Pessoa
        public PessoaController()
        {
            this._context = new Context();
        }

        public ActionResult Index()
        {
            //return View(_context.Pessoas.ToList());
            return View(_context.Pessoas.Where(a => a.Nome == "Douglas").ToList());

        }

        // GET: Pessoa/Details/5
        public ActionResult Details(int id)
        {
            return View(_context.Pessoas.Find(id));
        }

        // GET: Pessoa/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pessoa/Create
        [HttpPost]
        public ActionResult Create(Pessoa p)
        {
            try
            {
                // TODO: Add insert logic here
                _context.Pessoas.Add(p);
                _context.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Pessoa/Edit/5
        public ActionResult Edit(int id)
        {
            return View(_context.Pessoas.Find(id));
        }

        // POST: Pessoa/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Pessoa p)
        {
            try
            {
                // TODO: Add update logic here
                _context.Entry(p).State = EntityState.Modified;
                _context.SaveChanges();


                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Pessoa/Delete/5
        public ActionResult Delete(int id)
        {
            return View(_context.Pessoas.Find(id));
        }

        // POST: Pessoa/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Pessoa p)
        {
            try
            {
                // TODO: Add delete logic here
                Pessoa pessoa = _context.Pessoas.Find(p.Id);

                _context.Pessoas.Remove(pessoa);
                _context.SaveChanges();


                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
